﻿namespace Master.Domain
{
    public class CrawleredUrl
    {
        public string Url { get; set; } = string.Empty;
        public string? Title { get; set; }
        public string? Error { get; set; }
        public ProcessState State { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ProcessedDate { get; set; }
    }
}