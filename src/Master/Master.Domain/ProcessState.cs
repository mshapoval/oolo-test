﻿namespace Master.Domain
{
    public enum ProcessState: byte
    {
        New,
        InProgress,
        Completed,
        Failed
    }
}