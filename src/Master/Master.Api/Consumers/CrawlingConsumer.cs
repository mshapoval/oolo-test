﻿using EasyNetQ.AutoSubscribe;
using Master.Application.Endpoints.Crawling.Commands.SaveCrawlingUrlResult;
using Master.Contracts.Messages;
using MediatR;

namespace Master.Api.Consumers
{
    public class CrawlingConsumer : IConsumeAsync<UrlCrawlingCompletedEvent>
    {
        private readonly IMediator _mediator;

        public CrawlingConsumer(IMediator mediator)
        {
            _mediator = mediator;
        }

        public Task ConsumeAsync(UrlCrawlingCompletedEvent message, CancellationToken cancellationToken = default)
        {
            return _mediator.Send(new SaveCrawlingUrlResultCommand(message.Url, message.Title, message.Error, message.ProcessedDate), cancellationToken);
        }
    }
}
