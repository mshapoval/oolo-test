using Master.Application.Endpoints.Crawling.Commands.SaveUrls;
using Master.Application.Endpoints.Crawling.Queries.GetCrawledUrls;
using Master.Contracts.Dto;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace Master.Api.Controllers
{
    [ApiController]
    [Route("api/crawling")]
    public class CrawlingController : ControllerBase
    {
        private readonly IMediator _mediator;

        public CrawlingController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CrawlUrlsRequest request, CancellationToken cancellationToken)
        {
            await _mediator.Send(new SaveUrlsCommand(request?.Urls), cancellationToken);
            return Accepted();
        }

        [HttpGet("urls")]
        public async Task<IEnumerable<CrawledUrlsResponse>> Get([FromQuery] int? minutePeriod, CancellationToken cancellationToken)
        {
            var crawledUrls = await _mediator.Send(new GetCrawledUrlsQuery(minutePeriod ?? default), cancellationToken);
            return crawledUrls.GroupBy(x => new Uri(x.Url).GetComponents(UriComponents.SchemeAndServer, UriFormat.Unescaped))
                .Select(x => new CrawledUrlsResponse
                {
                    RootUrl = x.Key,
                    Urls = x.Select(x => new UrlResult { Url = x.Url, Title = x.Title, Error = x.Error }).ToList()
                });
        }
    }
}