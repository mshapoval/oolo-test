﻿using Master.Application.Common.Exceptions;
using Master.Application.Endpoints.Crawling.Exceptions;
using Master.Contracts.Dto;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;

namespace Master.Api.Infrastructure.Filters
{
    public class ExceptionFilter : IExceptionFilter
    {
        public void OnException(ExceptionContext context)
        {
            var result = context.Exception switch
            {
                ValidationException validation => new ObjectResult(new ErrorResponse { Message = validation.Message })
                {
                    StatusCode = (int)HttpStatusCode.BadRequest
                },
                CrawleredUrlNotFoundException notFound => new ObjectResult(new ErrorResponse { Message = notFound.Message })
                {
                    StatusCode = (int)HttpStatusCode.NotFound
                },
                _ => null
            };

            if(result != null)
            {
                context.Result = result;
            }
        }
    }
}
