﻿using Master.Contracts.Dto;
using Microsoft.AspNetCore.Diagnostics;
using System.Net;
using System.Text.Json;

namespace Master.Api.Infrastructure.Filters
{
    public class SystemExceptionHandler
    {
        public static Task Handler(HttpContext context)
        {
            if (context.Features[typeof(IExceptionHandlerFeature)] is IExceptionHandlerFeature exceptionFeature)
            {
                context.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                context.Response.ContentType = "application/json";
                context.Response.WriteAsync(JsonSerializer.Serialize(new ErrorResponse
                {
                    Message = "Oops, something went wrong"
                }));

                var logger = context.RequestServices.GetService<ILogger<SystemExceptionHandler>>();
                logger?.LogError(exceptionFeature.Error, "Unhandled exception");
            }

            return Task.CompletedTask;
        }
    }
}
