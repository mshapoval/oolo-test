﻿using Master.Application.Endpoints.Crawling.Commands.SendUrlsToCrawl;
using MediatR;

namespace Master.Api.HostedServices
{
    public class MessageRelayHostedService : IHostedService, IDisposable
    {
        private static readonly TimeSpan Period = TimeSpan.FromSeconds(1);
        private Timer _timer = null!;
        private readonly IServiceProvider _serviceProvider;

        public MessageRelayHostedService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(_ => DoWork(cancellationToken), null, TimeSpan.Zero, Period);

            return Task.CompletedTask;
        }

        private void DoWork(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0); // not to start new job if old one wasn't finished

            using var scope = _serviceProvider.CreateScope();
            var mediator = scope.ServiceProvider.GetRequiredService<IMediator>();
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<MessageRelayHostedService>>();
            try
            {
                mediator.Send(new SendUrlsToCrawlCommand(), cancellationToken).Wait();
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Message relay error");
            }

            _timer?.Change(Period, Period);
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }
}
