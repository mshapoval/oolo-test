using Master.Api.Consumers;
using Master.Api.HostedServices;
using Master.Api.Infrastructure.Filters;
using Master.Application;
using Master.Contracts.Messages;
using Master.Infrastructure;
using Microsoft.AspNetCore.Mvc;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<ApiBehaviorOptions>(options =>
{
    options.SuppressModelStateInvalidFilter = true; // we want to handle errors only on MediatR level
});
builder.Services.AddControllers(c =>
{
   c.Filters.Add<ExceptionFilter>();
});

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddMasterApplication(builder.Configuration);
builder.Services.AddMasterInfrastructure(builder.Configuration);
builder.Services.AddConsumer<CrawlingConsumer, UrlCrawlingCompletedEvent>();

builder.Services.AddHostedService<MessageRelayHostedService>();

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

app.MapControllers();

app.UseExceptionHandler(new ExceptionHandlerOptions
{
    ExceptionHandler = SystemExceptionHandler.Handler
});

app.Run();
