﻿namespace Master.Contracts.Messages
{
    public class UrlCrawlingCompletedEvent
    {
        public string Url { get; set; } = string.Empty;
        public string? Title { get; set; }
        public string? Error { get; set; }
        public DateTime ProcessedDate { get; set; }
    }
}
