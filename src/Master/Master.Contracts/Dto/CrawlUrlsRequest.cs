﻿namespace Master.Contracts.Dto
{
    public class CrawlUrlsRequest
    {
        public IReadOnlyCollection<string> Urls { get; set; } = new List<string>();
    }
}
