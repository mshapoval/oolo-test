﻿namespace Master.Contracts.Dto
{
    public class CrawledUrlsResponse
    {
        public string RootUrl { get; set; } = string.Empty;
        public IReadOnlyCollection<UrlResult> Urls { get; set; } = new List<UrlResult>();
    }

    public class UrlResult
    {
        public string Url { get; set; } = string.Empty;
        public string? Title { get; set; }
        public string? Error { get; set; }
    }
}
