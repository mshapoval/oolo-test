﻿namespace Master.Contracts.Dto
{
    public class ErrorResponse
    {
        public string Message { get; set; } = string.Empty;
    }
}
