﻿using Master.Application.Common.Persistence;
using Master.Application.Endpoints.Crawling.Exceptions;
using Master.Domain;
using System.Collections.Concurrent;

namespace Master.Infrastructure.Persistence
{
    internal class CrawleredUrlRepository : ICrawleredUrlRepository
    {
        private readonly ConcurrentDictionary<string, CrawleredUrl> _data = new();

        public Task<CrawleredUrl> Get(string url, CancellationToken cancellationToken = default)
        {
            if (!_data.TryGetValue(url, out var result))
            {
                throw new CrawleredUrlNotFoundException(url);
            }

            return Task.FromResult(Clone(result));
        }

        public Task<IReadOnlyCollection<CrawleredUrl>> GetAll(IReadOnlyCollection<string> urls, CancellationToken cancellationToken = default)
        {
            var result = urls.Where(_data.ContainsKey).Select(x => _data[x]).ToList();
            return Task.FromResult(Clone(result));
        }

        public Task<IReadOnlyCollection<CrawleredUrl>> GetAll(DateTime processedAfter, CancellationToken cancellationToken = default)
        {
            var results = _data.Values.Where(x => x.ProcessedDate > processedAfter).ToList();
            return Task.FromResult(Clone(results));
        }

        public Task<IReadOnlyCollection<CrawleredUrl>> GetAll(ProcessState state, CancellationToken cancellationToken = default)
        {
            var results = _data.Values.Where(x => x.State == state).ToList();
            return Task.FromResult(Clone(results));
        }

        public Task AddIfNotExists(IReadOnlyCollection<CrawleredUrl> crawleredUrls, CancellationToken cancellationToken = default)
        {
            foreach (var crawleredUrl in crawleredUrls)
            {
                _data.TryAdd(crawleredUrl.Url, Clone(crawleredUrl));
            }

            return Task.CompletedTask;
        }

        public Task Update(IReadOnlyCollection<CrawleredUrl> crawleredUrls, CancellationToken cancellationToken = default)
        {
            foreach (var crawleredUrl in crawleredUrls)
            {
                _data.TryUpdate(crawleredUrl.Url, Clone(crawleredUrl), _data[crawleredUrl.Url]);
            }

            return Task.CompletedTask;
        }

        // we have to clone input/output models to be sure that the application layer will not change data without the repository
        private static CrawleredUrl Clone(CrawleredUrl crawleredUrl)
        {
            return new CrawleredUrl
            {
                Url = crawleredUrl.Url,
                CreatedDate = crawleredUrl.CreatedDate,
                ProcessedDate = crawleredUrl.ProcessedDate,
                State = crawleredUrl.State,
                Title = crawleredUrl.Title,
                Error = crawleredUrl.Error
            };
        }

        private static IReadOnlyCollection<CrawleredUrl> Clone(IReadOnlyCollection<CrawleredUrl> crawleredUrls)
            => crawleredUrls.Select(Clone).ToList();
    }
}
