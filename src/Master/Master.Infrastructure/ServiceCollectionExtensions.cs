﻿using EasyNetQ;
using EasyNetQ.AutoSubscribe;
using Master.Application.Common.Communication;
using Master.Application.Common.Persistence;
using Master.Infrastructure.Communication;
using Master.Infrastructure.Persistence;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Master.Infrastructure
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMasterInfrastructure(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.RegisterEasyNetQ(configuration.GetConnectionString("RabbitMq"), services =>
            {
                services.Register<IConventions>(c => new CustomConventions(c.Resolve<ITypeNameSerializer>()));
            });
            serviceCollection.AddSingleton<ICrawleredUrlRepository, CrawleredUrlRepository>()
                .AddTransient<IProducer, Producer>();
            serviceCollection.AddHostedService<ConsumerHostedService>();

            return serviceCollection;
        }

        public static IServiceCollection AddConsumer<T, TMessage>(this IServiceCollection serviceCollection)
            where T : class, IConsumeAsync<TMessage>
            where TMessage : class
        {
            return serviceCollection.AddTransient<IConsumeAsync<TMessage>, T>();
        }
    }
}
