﻿using EasyNetQ;
using EasyNetQ.AutoSubscribe;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Reflection;

namespace Master.Infrastructure.Communication
{
    internal class ConsumerHostedService : IHostedService, IDisposable
    {
        private readonly IServiceProvider serviceProvider;
        private IDisposable? _subscription;

        public ConsumerHostedService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var autoSubscribe = new AutoSubscriber(serviceProvider.GetRequiredService<IBus>(), "master")
            {
                AutoSubscriberMessageDispatcher = new ServiceProviderMessageDispatcher(serviceProvider)
            };

            _subscription = await autoSubscribe.SubscribeAsync(Assembly.GetEntryAssembly()?.GetTypes());
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _subscription?.Dispose();
            _subscription = null;
        }
    }
}
