﻿using EasyNetQ;

namespace Master.Infrastructure.Communication
{
    internal class CustomConventions : Conventions
    {
        public CustomConventions(ITypeNameSerializer typeNameSerializer)
            : base(typeNameSerializer)
        {
            ErrorQueueNamingConvention = x => $"error_{x.Queue}";
            QueueNamingConvention = (type, subscription) => $"{subscription.Split(':').First()}_{type.Name}";
            ExchangeNamingConvention = type => type.Name;
        }
    }
}
