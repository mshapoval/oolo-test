﻿using EasyNetQ;
using Master.Application.Common.Communication;

namespace Master.Infrastructure.Communication
{
    internal class Producer : IProducer
    {
        private readonly IBus _bus;

        public Producer(IBus bus)
        {
            _bus = bus;
        }

        public async Task Produce<T>(IReadOnlyCollection<T> messages, CancellationToken cancellationToken = default)
        {
            foreach (var message in messages)
            {
                await _bus.PubSub.PublishAsync(message, cancellationToken);
            }
        }
    }
}

