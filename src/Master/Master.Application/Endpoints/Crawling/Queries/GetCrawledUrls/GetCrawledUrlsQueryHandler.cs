﻿using Master.Application.Common.Persistence;
using Master.Application.Endpoints.Crawling.Options;
using Master.Domain;
using MediatR;
using Microsoft.Extensions.Options;

namespace Master.Application.Endpoints.Crawling.Queries.GetCrawledUrls
{
    internal class GetCrawledUrlsQueryHandler : IRequestHandler<GetCrawledUrlsQuery, IReadOnlyCollection<CrawleredUrl>>
    {
        private readonly ICrawleredUrlRepository _repository;
        private readonly IOptionsSnapshot<FilterOptions> _options;

        public GetCrawledUrlsQueryHandler(ICrawleredUrlRepository repository, IOptionsSnapshot<FilterOptions> options)
        {
            _repository = repository;
            _options = options;
        }

        public Task<IReadOnlyCollection<CrawleredUrl>> Handle(GetCrawledUrlsQuery request, CancellationToken cancellationToken)
        {
            var filterPeriod = request.MinutePeriod <= 0 ? _options.Value.DefaultFilterPeriod : TimeSpan.FromMinutes(request.MinutePeriod);
            var after = DateTime.UtcNow.Add(-filterPeriod);
            return _repository.GetAll(after, cancellationToken);
        }
    }
}
