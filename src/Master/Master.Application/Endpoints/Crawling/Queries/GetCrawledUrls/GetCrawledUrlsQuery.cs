﻿using Master.Domain;
using MediatR;

namespace Master.Application.Endpoints.Crawling.Queries.GetCrawledUrls
{
    public record GetCrawledUrlsQuery(int MinutePeriod) : IRequest<IReadOnlyCollection<CrawleredUrl>>;
}
