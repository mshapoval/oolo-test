﻿namespace Master.Application.Endpoints.Crawling.Options
{
    internal class FilterOptions
    {
        public TimeSpan DefaultFilterPeriod { get; set; }
    }
}
