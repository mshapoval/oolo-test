﻿namespace Master.Application.Endpoints.Crawling.Exceptions
{
    public class CrawleredUrlNotFoundException : Exception
    {
        public CrawleredUrlNotFoundException(string url)
            : base($"{url} was not crawlered")
        {
        }
    }
}
