﻿using Master.Application.Common.Persistence;
using Master.Domain;
using MediatR;

namespace Master.Application.Endpoints.Crawling.Commands.SaveCrawlingUrlResult
{
    internal class SaveCrawlingUrlResultCommandHandler : AsyncRequestHandler<SaveCrawlingUrlResultCommand>
    {
        private readonly ICrawleredUrlRepository _repository;

        public SaveCrawlingUrlResultCommandHandler(ICrawleredUrlRepository repository)
        {
            _repository = repository;
        }

        protected override async Task Handle(SaveCrawlingUrlResultCommand request, CancellationToken cancellationToken)
        {
            var crawledUrl = await _repository.Get(request.Url, cancellationToken);
            crawledUrl.Title = request.Title;
            crawledUrl.Error = request.Error;
            crawledUrl.ProcessedDate = request.ProcessedDate;
            crawledUrl.State = !string.IsNullOrWhiteSpace(request.Title) ? ProcessState.Completed : ProcessState.Failed;

            await _repository.Update(new[] { crawledUrl }, cancellationToken);
        }
    }
}
