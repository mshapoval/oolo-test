﻿using MediatR;

namespace Master.Application.Endpoints.Crawling.Commands.SaveCrawlingUrlResult
{
    public record SaveCrawlingUrlResultCommand(string Url, string? Title, string? Error, DateTime ProcessedDate) : IRequest;
}
