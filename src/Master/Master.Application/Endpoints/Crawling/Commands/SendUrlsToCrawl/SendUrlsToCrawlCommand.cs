﻿using MediatR;

namespace Master.Application.Endpoints.Crawling.Commands.SendUrlsToCrawl
{
    public record SendUrlsToCrawlCommand : IRequest;
}
