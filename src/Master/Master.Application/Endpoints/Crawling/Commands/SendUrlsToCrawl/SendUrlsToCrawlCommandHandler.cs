﻿using Master.Application.Common.Communication;
using Master.Application.Common.Persistence;
using Master.Contracts.Messages;
using Master.Domain;
using MediatR;

namespace Master.Application.Endpoints.Crawling.Commands.SendUrlsToCrawl
{
    internal class SendUrlsToCrawlCommandHandler : AsyncRequestHandler<SendUrlsToCrawlCommand>
    {
        private readonly ICrawleredUrlRepository _repository;
        private readonly IProducer _producer;

        public SendUrlsToCrawlCommandHandler(ICrawleredUrlRepository repository, IProducer producer)
        {
            _repository = repository;
            _producer = producer;
        }

        protected override async Task Handle(SendUrlsToCrawlCommand _, CancellationToken cancellationToken)
        {
            var now = DateTime.UtcNow;
            var urls = await _repository.GetAll(ProcessState.New);
            var messages = urls.Select(x => new UrlCrawlingRequestedEvent { Url = x.Url })
                .ToList();

            if (!messages.Any())
            {
                return;
            }

            await _producer.Produce(messages);

            // this is an unlikely scenario, but if database becames unavailable in this point, we will send duplicate messages during next iteration
            foreach (var url in urls)
            {
                url.State = ProcessState.InProgress;
            }

            await _repository.Update(urls);
        }
    }
}
