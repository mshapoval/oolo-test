﻿using MediatR;

namespace Master.Application.Endpoints.Crawling.Commands.SaveUrls
{
    public record SaveUrlsCommand(IReadOnlyCollection<string>? Urls) : IRequest;
}
