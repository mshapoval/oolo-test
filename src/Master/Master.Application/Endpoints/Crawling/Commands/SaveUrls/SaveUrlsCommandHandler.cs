﻿using Master.Application.Common.Persistence;
using Master.Domain;
using MediatR;

namespace Master.Application.Endpoints.Crawling.Commands.SaveUrls
{
    internal class SaveUrlsCommandHandler : AsyncRequestHandler<SaveUrlsCommand>
    {
        private readonly ICrawleredUrlRepository _repository;

        public SaveUrlsCommandHandler(ICrawleredUrlRepository repository)
        {
            _repository = repository;
        }

        protected override async Task Handle(SaveUrlsCommand request, CancellationToken cancellationToken)
        {
            var now = DateTime.UtcNow;
            var urlToStore = request.Urls.ToHashSet() // remove duplicates
                .Select(x => new CrawleredUrl
                {
                    Url = x,
                    CreatedDate = now,
                    State = ProcessState.New
                }).ToList();

            // here we use Transactional outbox pattern - https://microservices.io/patterns/data/transactional-outbox.html
            // it guarantees that after we save data, we will send the event at least once 
            // it's better to use the separated table for outbox events, but for this app, it's easier to use only one table
            await _repository.AddIfNotExists(urlToStore);
        }
    }
}
