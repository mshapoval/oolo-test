﻿using FluentValidation;

namespace Master.Application.Endpoints.Crawling.Commands.SaveUrls
{
    internal class SaveUrlsCommandValidator : AbstractValidator<SaveUrlsCommand>
    {
        private static IReadOnlyCollection<string> AllowedSchemas = new[] { "http", "https" };
        public SaveUrlsCommandValidator()
        {
            RuleFor(x => x.Urls).NotEmpty();
            RuleForEach(x => x.Urls).NotEmpty().MaximumLength(2048).Must(x => IsValidUrl(x)).WithMessage("Invalid url");
        }

        private static bool IsValidUrl(string url) => Uri.TryCreate(url, UriKind.Absolute, out var uri) && AllowedSchemas.Contains(uri.Scheme);
    }
}
