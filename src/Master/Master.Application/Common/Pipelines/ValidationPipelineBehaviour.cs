﻿using FluentValidation;
using MediatR;

namespace Master.Application.Common.Pipelines
{
    internal class ValidationPipelineBehaviour<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
        where TRequest : IRequest<TResponse>
    {
        private readonly IEnumerable<IValidator<TRequest>> _validators;

        public ValidationPipelineBehaviour(IEnumerable<IValidator<TRequest>> validators)
        {
            _validators = validators;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            if (_validators.Any())
            {
                var context = new ValidationContext<TRequest>(request);
                var errorMessages = new List<string>();
                foreach (var validator in _validators)
                {
                    var result = await validator.ValidateAsync(context, cancellationToken);
                    var errors = result.Errors.Where(x => x is not null).Select(x => $"{x.PropertyName}: {x.ErrorMessage}");
                    errorMessages.AddRange(errors);
                }
                if (errorMessages.Any())
                {
                    var message = string.Join(Environment.NewLine, errorMessages);
                    throw new Exceptions.ValidationException(message);
                }
            }
            return await next();
        }
    }
}
