﻿using Master.Domain;

namespace Master.Application.Common.Persistence
{
    public interface ICrawleredUrlRepository
    {
        Task<CrawleredUrl> Get(string url, CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<CrawleredUrl>> GetAll(IReadOnlyCollection<string> urls, CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<CrawleredUrl>> GetAll(DateTime processedAfter, CancellationToken cancellationToken = default);
        Task<IReadOnlyCollection<CrawleredUrl>> GetAll(ProcessState state, CancellationToken cancellationToken = default);
        Task AddIfNotExists(IReadOnlyCollection<CrawleredUrl> crawleredUrls, CancellationToken cancellationToken = default);
        Task Update(IReadOnlyCollection<CrawleredUrl> crawleredUrls, CancellationToken cancellationToken = default);
    }
}
