﻿namespace Master.Application.Common.Communication
{
    public interface IProducer
    {
        Task Produce<T>(IReadOnlyCollection<T> messages, CancellationToken cancellationToken = default);
    }
}
