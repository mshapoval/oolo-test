﻿using FluentValidation;
using Master.Application.Common.Pipelines;
using Master.Application.Endpoints.Crawling.Commands.SaveUrls;
using Master.Application.Endpoints.Crawling.Options;
using MediatR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Master.Application
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddMasterApplication(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.AddMediatR(typeof(SaveUrlsCommand).Assembly)
                .AddScoped(typeof(IPipelineBehavior<,>), typeof(ValidationPipelineBehaviour<,>));

            serviceCollection.AddScoped<IValidator<SaveUrlsCommand>, SaveUrlsCommandValidator>();

            serviceCollection.Configure<FilterOptions>(configuration.GetSection("Filter"));
            return serviceCollection;
        }
    }
}
