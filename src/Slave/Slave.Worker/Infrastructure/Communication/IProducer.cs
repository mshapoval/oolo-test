﻿namespace Slave.Worker.Infrastructure.Communication
{
    public interface IProducer
    {
        Task Produce<T>(T message, CancellationToken cancellationToken = default);
    }
}
