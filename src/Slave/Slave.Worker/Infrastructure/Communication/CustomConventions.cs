﻿using EasyNetQ;

namespace Slave.Worker.Infrastructure.Communication
{
    internal class CustomConventions : Conventions
    {
        public CustomConventions(ITypeNameSerializer typeNameSerializer)
            : base(typeNameSerializer)
        {
            ErrorQueueNamingConvention = x => $"error_{x.Queue}";
            QueueNamingConvention = (type, subsription) => $"{subsription.Split(':').First()}_{type.Name}";
            ExchangeNamingConvention = type => type.Name;
        }
    }
}
