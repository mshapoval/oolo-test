﻿using EasyNetQ;

namespace Slave.Worker.Infrastructure.Communication
{
    internal class Producer : IProducer
    {
        private readonly IBus _bus;

        public Producer(IBus bus)
        {
            _bus = bus;
        }

        public Task Produce<T>(T message, CancellationToken cancellationToken = default)
        {
            return _bus.PubSub.PublishAsync(message, cancellationToken);
        }
    }
}

