﻿using EasyNetQ.AutoSubscribe;

namespace Slave.Worker.Infrastructure.Communication
{
    public class ServiceProviderMessageDispatcher : IAutoSubscriberMessageDispatcher
    {
        private readonly IServiceProvider _serviceProvider;

        public ServiceProviderMessageDispatcher(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public void Dispatch<TMessage, TConsumer>(TMessage message, CancellationToken cancellationToken) where TMessage : class where TConsumer : class, IConsume<TMessage>
        {
            throw new NotSupportedException("Only async consumers are supported");
        }

        public Task DispatchAsync<TMessage, TConsumer>(TMessage message, CancellationToken cancellationToken) where TMessage : class where TConsumer : class, IConsumeAsync<TMessage>
        {
            using var scope = _serviceProvider.CreateScope();
            var consumer = scope.ServiceProvider.GetRequiredService<IConsumeAsync<TMessage>>();
            var logger = scope.ServiceProvider.GetRequiredService<ILogger<ServiceProviderMessageDispatcher>>();

            try
            {
                return consumer.ConsumeAsync(message, CancellationToken.None);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Cannot consume message {typeof(TMessage).Name}");
                throw;
            }
        }
    }
}
