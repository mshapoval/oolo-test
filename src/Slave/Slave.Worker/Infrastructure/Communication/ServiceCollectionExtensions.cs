﻿using EasyNetQ;
using EasyNetQ.AutoSubscribe;

namespace Slave.Worker.Infrastructure.Communication
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddSlaveInfrastructure(this IServiceCollection serviceCollection, IConfiguration configuration)
        {
            serviceCollection.RegisterEasyNetQ(configuration.GetConnectionString("RabbitMq"), services =>
            {
                services.Register<IConventions>(c => new CustomConventions(c.Resolve<ITypeNameSerializer>()));
            });
            serviceCollection.AddHostedService<ConsumerHostedService>();
            serviceCollection.AddTransient<IProducer, Producer>();

            return serviceCollection;
        }

        public static IServiceCollection AddConsumer<T, TMessage>(this IServiceCollection serviceCollection)
            where T : class, IConsumeAsync<TMessage>
            where TMessage : class
        {
            return serviceCollection.AddTransient<IConsumeAsync<TMessage>, T>();
        }
    }
}
