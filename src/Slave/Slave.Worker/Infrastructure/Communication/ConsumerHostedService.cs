﻿using EasyNetQ;
using EasyNetQ.AutoSubscribe;
using System.Reflection;

namespace Slave.Worker.Infrastructure.Communication
{
    internal class ConsumerHostedService : IHostedService, IDisposable
    {
        private readonly IServiceProvider serviceProvider;
        private IDisposable? _subscription;

        public ConsumerHostedService(IServiceProvider serviceProvider)
        {
            this.serviceProvider = serviceProvider;
        }

        public async Task StartAsync(CancellationToken cancellationToken)
        {
            var autoSubscribe = new AutoSubscriber(serviceProvider.GetRequiredService<IBus>(), "slave")
            {
                AutoSubscriberMessageDispatcher = new ServiceProviderMessageDispatcher(serviceProvider),
                ConfigureSubscriptionConfiguration = c => c.WithPrefetchCount(20)
            };

            _subscription = await autoSubscribe.SubscribeAsync(Assembly.GetEntryAssembly()?.GetTypes());
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _subscription?.Dispose();
            _subscription = null;
        }
    }
}
