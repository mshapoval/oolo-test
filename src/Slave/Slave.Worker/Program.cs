using Master.Api.Consumers;
using Master.Contracts.Messages;
using Polly;
using Polly.Extensions.Http;
using Polly.Timeout;
using Slave.Worker.Infrastructure.Communication;
using Slave.Worker.Services;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddSlaveInfrastructure(builder.Configuration);
builder.Services.AddConsumer<CrawlingConsumer, UrlCrawlingRequestedEvent>();

builder.Services.AddHttpClient<ICrawlService, CrawlService>()
    .AddPolicyHandler(
        HttpPolicyExtensions
        .HandleTransientHttpError()
        .Or<TimeoutRejectedException>()
        .WaitAndRetryAsync(3, _ => TimeSpan.FromSeconds(5)) // retry configuration should be in settings
                                                            // the sleep duration provider shound not return static interval
    );

var app = builder.Build();
app.Run();
