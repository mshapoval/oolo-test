﻿using EasyNetQ.AutoSubscribe;
using Master.Contracts.Messages;
using Slave.Worker.Services;

namespace Master.Api.Consumers
{
    public class CrawlingConsumer : IConsumeAsync<UrlCrawlingRequestedEvent>
    {
        private readonly ICrawlService _crawlService;

        public CrawlingConsumer(ICrawlService crawlService)
        {
            _crawlService = crawlService;
        }

        public Task ConsumeAsync(UrlCrawlingRequestedEvent message, CancellationToken cancellationToken = default)
        {
            return _crawlService.Crawl(message.Url, cancellationToken);
        }
    }
}
