﻿using Master.Contracts.Messages;
using Slave.Worker.Infrastructure.Communication;
using System.Text.RegularExpressions;
using System.Web;

namespace Slave.Worker.Services
{
    public class CrawlService : ICrawlService
    {
        private readonly HttpClient _httpClient;
        private readonly IProducer _producer;

        public CrawlService(HttpClient httpClient, IProducer producer)
        {
            _httpClient = httpClient;
            _producer = producer;
        }

        public async Task Crawl(string url, CancellationToken cancellationToken)
        {
            var result = await GetTitle(url, cancellationToken);
            await _producer.Produce(new UrlCrawlingCompletedEvent
            {
                Url = url,
                ProcessedDate = DateTime.UtcNow,
                Title = result.Title,
                Error = result.Error
            }, cancellationToken);
        }

        private async Task<(string? Error, string? Title)> GetTitle(string url, CancellationToken cancellationToken)
        {
            HttpResponseMessage response;
            try
            {
                response = await _httpClient.GetAsync(url, cancellationToken);
            }
            catch (Exception ex)
            {
                return (ex.Message, null);
            }

            if (!response.IsSuccessStatusCode)
            {
                return ($"Not success status code: {response.StatusCode}", null);
            }

            if (response.Content.Headers.ContentType?.MediaType != "text/html")
            {
                return ("It is not a web page", null);
            }

            var page = await response.Content.ReadAsStringAsync();
            var title = Regex.Match(page, @"\<title\b[^>]*\>\s*(?<Title>[\s\S]*?)\</title\>", RegexOptions.IgnoreCase).Groups["Title"].Value; // just found this regex in Google :)
            title = !string.IsNullOrWhiteSpace(title) ? HttpUtility.HtmlDecode(title) : title;
            return (null, title);
        }
    }
}
