﻿namespace Slave.Worker.Services
{
    public interface ICrawlService
    {
        Task Crawl(string url, CancellationToken cancellationToken);
    }
}
