﻿namespace Master.Contracts.Messages
{
    public class UrlCrawlingRequestedEvent
    {
        public string Url { get; set; } = string.Empty;
    }
}
