# oolo test task



## Run application

```
docker-compose up
```
It will up: 1 master app, 2 slave workers and rabbitMq.\
Swagger for testing purpose - [click](http://localhost:5252/swagger)\
RabbitMq - [click](http://localhost:15672/)



## Architecture
![Architecture](docs/architecture.png)



## Master API

### Technologies and libraries
- .Net 6
- MediatR - entry point to the application level
- FluentValidation - integrated with MeditR
- EasyNetQ - RabbitMq integration framework
### Design
For the master app I chose Clean Architecture approach.\
![Clean Architecture](docs/clean-architecture.png)


## Slave Worker

### Technologies and libraries
- .Net 6 - I started to develop it on Go, but it needs a lot of time to adapt RabbitMq packages to EasyNetQ conventions
- EasyNetQ - RabbitMq integration framework
- Polly - retry handling
### Design
Just lightweight worker with consumer-centric design.
